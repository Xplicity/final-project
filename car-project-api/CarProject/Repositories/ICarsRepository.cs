﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarProject.Models;

namespace CarProject.Repositories
{
    public interface ICarsRepository
    {
        Task<Car> GetById(int id);
        Task<ICollection<Car>> GetAll(int offset, int limit);
        Task<int> Create(Car newCar);
        Task<Car> Update(Car updatedCar);
    }
}