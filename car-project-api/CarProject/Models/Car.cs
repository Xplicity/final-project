using System.ComponentModel.DataAnnotations;

namespace CarProject.Models
{
    public class Car
    {
        [Key]
        public int Id { get; set; }
        public string Model { get; set; }
        public string OtherInformation { get; set; }
        public string ImageUrl { get; set; }
    }
}