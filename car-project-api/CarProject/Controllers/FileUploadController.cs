using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarProject.Dtos;
using CarProject.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CarProject.Controllers
{
    [Route("api/fileUpload")]
    public class FileUploadController : Controller
    {
        private readonly IFileSaveService _fileService;

        public FileUploadController(IFileSaveService fileService)
        {
            _fileService = fileService;
        }

        [HttpPost]
        [Produces(typeof(FileUploadResultDto))]
        public async Task<IActionResult> Post(List<IFormFile> files)
        {
            if (files.Count == 0)
            {
                return BadRequest("Missing file. Are you missing form-data file key?");
            }

            var result = await _fileService.Save(files.FirstOrDefault());

            return Ok(result);
        }
    }
}