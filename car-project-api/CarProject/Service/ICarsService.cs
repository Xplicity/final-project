﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarProject.Models;

namespace CarProject.Service
{
    public interface ICarsService
    {
        Task<Car> GetCarById(int id);

        Task<ICollection<Car>> GetAllCars(int offset, int limit);

        Task<int> Create(Car car);

        Task<Car> Update(Car car);
    }
}
