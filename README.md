# Final Project

## Task
Pagrindinė užduotis:

Esamą mašinų įvedimo programą praplėsti pridedant variklių ir padangų pasirinkimo funkcionalumą. 

* Variklis turi pavadinimą, galią ir darbinį turį. 
* Padangos turi skersmenį, plotį, aukstį ir pavadinimą. 

Programoje turėtų būti galima sukurti variklius ir padangas, taip pat jau įvestus variklius ir padangas priskirti mašinai. Mašinos variklį ir padangas galima keisti bet kuriuo metu. 
Aukščiau išvardintos funkcijos turi būti prieinamos per Angular programą. 

Papildomos užduotys:

Jei kitas vartotojas pakeitė duomenis, kurie šiuo metu atvaizduojami ekrane, esamas vartotojas turėtų būti informuotas apie pakeitimus arba pakeitimai turi būti atvaizduoti automatiškai. 
Angular programai turi būti suteiktas vientisas stilius.

## Delivery process

Clone the repository and create a branch in it named "YourName-YourSurname" and push it immediately. Continue implementing the tasks in your branch.
When you're done or the time is up, push it in a single commit. The required git commands are listed below.

### Git commands
```
git clone https://gitlab.com/Xplicity/final-project.git
cd final-project
git checkout -b name-surname
git push origin name-surname
```