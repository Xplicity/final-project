﻿using CarProject.Models;
using Microsoft.EntityFrameworkCore;

namespace CarProject.Repositories
{
    //EntityFrameworkCore sqlite
    public class CarsDbContext : DbContext
    {
        public DbSet<Car> Cars { get; set; }

        public CarsDbContext(DbContextOptions options) :
            base(options)
        {

        }

        //to configure db
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}
