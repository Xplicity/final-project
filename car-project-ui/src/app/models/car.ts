export class Car {
  id: number;
  model: string;
  otherInformation: string;
  imageUrl: string;
}
