import {Component, OnInit} from '@angular/core';
import {CarService} from '../../services/car.service';
import {Car} from '../../models/car';
import {Router} from '@angular/router';

@Component({
  selector: 'app-car-list',
  templateUrl: './car-list.component.html',
  styleUrls: ['./car-list.component.css']
})
export class CarListComponent implements OnInit {

  cars: Car[] = [];

  newCar: Car = new Car();

  constructor(private carService: CarService, private router: Router) {
  }

  ngOnInit() {
    this.carService.getCars().subscribe(cars => {
        this.cars = cars;
        console.log(cars)
      },
      error => {
        console.log(error);
      });
  }

  addNewCar(car: Car) {
    this.carService.addCar(car).subscribe((carId: number) => {
        car.id = carId;
        this.cars.push(car);
      });
  }

  onCarRowClick(car: Car) {
    this.router.navigate(['/cars', car.id]);
  }

}
