﻿using CarProject.Repositories;
using CarProject.Service;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace CarProject
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddAllDependencies(this IServiceCollection service)
        {
            return service.AddScoped<ICarsService, CarsService>()
                .AddScoped<IFileSaveService, FileSaveService>()
                .AddScoped<ICarsRepository, CarsRepository>()
                .AddDbContext<CarsDbContext>(options => options.UseSqlite("Filename=./demo.db") );
        }
    }
}
