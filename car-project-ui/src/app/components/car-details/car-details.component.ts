import {Component, Input, OnInit} from '@angular/core';
import {Car} from '../../models/car';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {CarService} from '../../services/car.service';

@Component({
  selector: 'app-car-details',
  templateUrl: './car-details.component.html',
  styleUrls: ['./car-details.component.css']
})
export class CarDetailsComponent implements OnInit {

  car: Car;

  constructor(private route: ActivatedRoute, private router: Router,
              private carService: CarService) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      const id = params.get('id'); // id from URL
      this.carService.getCar(id).subscribe((car: Car) => {
        this.car = car;
      });
    });
  }

  onBackClick() {
    this.router.navigate(['cars']);
  }

  getImageUrl(): string {
    return this.carService.getCarImageUrl(this.car.imageUrl);
  }
}
