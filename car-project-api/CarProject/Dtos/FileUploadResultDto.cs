﻿namespace CarProject.Dtos
{
    public class FileUploadResultDto
    {
        public long Size { get; set; }
        public string FileName { get; set; }
    }
}