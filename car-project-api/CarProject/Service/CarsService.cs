﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarProject.Models;
using CarProject.Repositories;

namespace CarProject.Service
{
    public class CarsService : ICarsService
    {
        private readonly ICarsRepository _repository;

        public CarsService(ICarsRepository repository)
        {
            _repository = repository;
        }

        public Task<Car> GetCarById(int id)
        {
            var car = _repository.GetById(id);
            return car;
        }

        public Task<ICollection<Car>> GetAllCars(int offset, int limit)
        {
            var cars = _repository.GetAll(offset, limit);
            return cars;
        }

        public async Task<int> Create(Car car)
        {
            var newId = await _repository.Create(car);

            return newId;
        }

        public async Task<Car> Update(Car car)
        {
            var updatedCar = await _repository.Update(car);

            return updatedCar;
        }
    }
}