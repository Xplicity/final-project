﻿using System.Threading.Tasks;
using CarProject.Models;
using CarProject.Service;
using Microsoft.AspNetCore.Mvc;

namespace CarProject.Controllers
{
    [Route("api/cars")]
    public class CarsController: Controller
    {
        private readonly ICarsService _service;

        public CarsController(ICarsService service)
        {
            _service = service;
        }

        [HttpGet]
        [Produces(typeof(Car))]
        public async Task<IActionResult> GetRange([FromQuery]int offset = 0, [FromQuery]int limit = 10)
        {
            var cars = await _service.GetAllCars(offset, limit);
            return Ok(cars);
        }

        [HttpGet("{id}")]
        [Produces(typeof(Car[]))]
        public async Task<IActionResult> Get([FromRoute]int id)
        {
            var result = await _service.GetCarById(id);

            if (result == null)
            {
                return NotFound("Usefull data why it was not found");
            }

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Car value)
        {
            var id = await _service.Create(value);
            return Ok(id);
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] Car car)
        {
            var updatedCar = await _service.Update(car);

            return Ok(updatedCar);
        }
    }
}
