import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {CarListComponent} from './components/car-list/car-list.component';
import {CarService} from './services/car.service';
import {FormsModule} from '@angular/forms';
import {CarInputComponent} from './components/car-input/car-input.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {RouterModule, Routes} from '@angular/router';
import {AboutComponent} from './components/about/about.component';
import {CarDetailsComponent} from './components/car-details/car-details.component';
import {MdButtonModule, MdIconModule, MdInputModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

const appRoutes: Routes = [
  {path: 'cars', component: CarListComponent},
  {path: 'about', component: AboutComponent},
  {path: 'cars/:id', component: CarDetailsComponent},
  {path: '', redirectTo: 'cars', pathMatch: 'full'}, // default page
  {path: '**', component: PageNotFoundComponent} // 404 page
];

@NgModule({
  declarations: [
    AppComponent,
    CarListComponent,
    CarInputComponent,
    PageNotFoundComponent,
    AboutComponent,
    CarDetailsComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    MdInputModule,
    MdButtonModule,
    MdIconModule
  ],
  providers: [
    CarService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
