﻿using System.Threading.Tasks;
using CarProject.Dtos;
using Microsoft.AspNetCore.Http;

namespace CarProject.Service
{
    public interface IFileSaveService
    {
        Task<FileUploadResultDto> Save(IFormFile file);
    }
}